# État de la traduction (npc-gallery)

 * **libre**: 46
 * **changé**: 1
 * **officielle**: 52


Dernière mise à jour: 2024-02-25 17:57 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0Kb4z4h8KVqfrIju.htm](npc-gallery/0Kb4z4h8KVqfrIju.htm)|Assassin|Assassin|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0Ex7rBuiJVu2NwCz.htm](npc-gallery/0Ex7rBuiJVu2NwCz.htm)|Demonologist|Démonologiste|libre|
|[1iz7O6DLDJqStojd.htm](npc-gallery/1iz7O6DLDJqStojd.htm)|Servant|Serviteur|libre|
|[1NZ1ZAgcUlWKmQSs.htm](npc-gallery/1NZ1ZAgcUlWKmQSs.htm)|Bosun|Bosco|officielle|
|[1sWw5OgmpazLVqRQ.htm](npc-gallery/1sWw5OgmpazLVqRQ.htm)|Mage For Hire|Magicien à louer|libre|
|[1U1URD7IyddoD5zE.htm](npc-gallery/1U1URD7IyddoD5zE.htm)|Fence|Receleur|libre|
|[2SBKFCog4JY3WrOW.htm](npc-gallery/2SBKFCog4JY3WrOW.htm)|Rain-Scribe|Scribe de la pluie|libre|
|[3lZhmvNLQkiYGAof.htm](npc-gallery/3lZhmvNLQkiYGAof.htm)|Sage|Sage|libre|
|[401MnHX5aO21P2Y8.htm](npc-gallery/401MnHX5aO21P2Y8.htm)|Stone Giant Monk|Moine géant de pierre|officielle|
|[48bZvtRcd7T6FmA7.htm](npc-gallery/48bZvtRcd7T6FmA7.htm)|Privateer Captain|Capitaine corsaire|libre|
|[51E3fdESgGjQxcMv.htm](npc-gallery/51E3fdESgGjQxcMv.htm)|Astronomer|Astronome|libre|
|[5LvpvIMhaYLcyAk6.htm](npc-gallery/5LvpvIMhaYLcyAk6.htm)|Pirate|Pirate|libre|
|[5Rqh2dBxGU8Jwf56.htm](npc-gallery/5Rqh2dBxGU8Jwf56.htm)|Azarketi Sailor|Marin azarketi|libre|
|[6IADTZHYowxObqAk.htm](npc-gallery/6IADTZHYowxObqAk.htm)|Barkeep|Tavernier|libre|
|[7eJJIIVEDB3EFFcZ.htm](npc-gallery/7eJJIIVEDB3EFFcZ.htm)|Bodyguard|Garde du corps|officielle|
|[7GGHuOlSzcaF2AdL.htm](npc-gallery/7GGHuOlSzcaF2AdL.htm)|Charlatan|Charlatan|libre|
|[7RF95b3WHkvHWLrv.htm](npc-gallery/7RF95b3WHkvHWLrv.htm)|Physician|Médecin|libre|
|[8coHofIpLa5ZnjAF.htm](npc-gallery/8coHofIpLa5ZnjAF.htm)|Navigator|Navigateur|officielle|
|[8WFGygPv7UHh7zdJ.htm](npc-gallery/8WFGygPv7UHh7zdJ.htm)|Apothecary|Apothicaire|libre|
|[95IcOUvxABvj5lvo.htm](npc-gallery/95IcOUvxABvj5lvo.htm)|Changeling Hellknight|Chevalier infernal changelin|libre|
|[9jDT7EhtlZtNpCz7.htm](npc-gallery/9jDT7EhtlZtNpCz7.htm)|False Priest|Faux prêtre|libre|
|[aJR3f8YcAkmfx7im.htm](npc-gallery/aJR3f8YcAkmfx7im.htm)|Apprentice|Apprenti|libre|
|[Ap87yR4WOs0wKHx7.htm](npc-gallery/Ap87yR4WOs0wKHx7.htm)|Cultist|Cultiste|officielle|
|[B09JfuBZHjcRXztU.htm](npc-gallery/B09JfuBZHjcRXztU.htm)|Burglar|Cambrioleur|officielle|
|[B0pZAGooj735FGfw.htm](npc-gallery/B0pZAGooj735FGfw.htm)|Tomb Raider|Écumeur de tombeaux|officielle|
|[B13dyXTo1xWVyj2R.htm](npc-gallery/B13dyXTo1xWVyj2R.htm)|Palace Guard|Garde de palais|officielle|
|[bc1jeTvmzKeYGVw9.htm](npc-gallery/bc1jeTvmzKeYGVw9.htm)|Virtuous Defender|Défenseur vertueux|libre|
|[cAOWcPIjtZYXYZ3i.htm](npc-gallery/cAOWcPIjtZYXYZ3i.htm)|Merchant|Marchand|libre|
|[Cnm5zWmuTEYy6mPx.htm](npc-gallery/Cnm5zWmuTEYy6mPx.htm)|Mastermind|Cerveau|libre|
|[crTWewxna93vEt6B.htm](npc-gallery/crTWewxna93vEt6B.htm)|Acolyte of Nethys|Acolyte de Néthys|libre|
|[DD2JNeRxO79WFlOL.htm](npc-gallery/DD2JNeRxO79WFlOL.htm)|Archer Sentry|Sentinelle archère|officielle|
|[DFurZlcpcNrUmmER.htm](npc-gallery/DFurZlcpcNrUmmER.htm)|Ruffian|Voyou|officielle|
|[DSA03902sWGot0ev.htm](npc-gallery/DSA03902sWGot0ev.htm)|Miner|Mineur|officielle|
|[EMl8hARVJk8SNVyW.htm](npc-gallery/EMl8hARVJk8SNVyW.htm)|Charming Scoundrel|Charmant vaurien|officielle|
|[EslFhpdvQf7KN8W3.htm](npc-gallery/EslFhpdvQf7KN8W3.htm)|Chronicler|Chroniqueur|libre|
|[EzD6YlNXL48rN8nq.htm](npc-gallery/EzD6YlNXL48rN8nq.htm)|Despot|Tyran|libre|
|[F8AzuPOCcveWasza.htm](npc-gallery/F8AzuPOCcveWasza.htm)|Ethereal Sailor|Marin éthéré|officielle|
|[G2ftdkyJ5WDonL0C.htm](npc-gallery/G2ftdkyJ5WDonL0C.htm)|Urchin|Gamin des rues|officielle|
|[GoGNtiHuYycppLPk.htm](npc-gallery/GoGNtiHuYycppLPk.htm)|Bounty Hunter|Chasseur de primes|officielle|
|[GRtEwNQgKQ9j9JPK.htm](npc-gallery/GRtEwNQgKQ9j9JPK.htm)|Warden|Gardien|libre|
|[gzirsGA07yG6CaG8.htm](npc-gallery/gzirsGA07yG6CaG8.htm)|Jailer|Geôlier|officielle|
|[hK8Tpg3baKWzmPEv.htm](npc-gallery/hK8Tpg3baKWzmPEv.htm)|Tax Collector|Percepteur|libre|
|[Hle05FibgOeZr7wF.htm](npc-gallery/Hle05FibgOeZr7wF.htm)|Hunter|Chasseur|officielle|
|[hxyImo4ts3O0BrAY.htm](npc-gallery/hxyImo4ts3O0BrAY.htm)|Veteran Reclaimer|Reconquérant vétéran|officielle|
|[IaSxoVNZFYatdfjI.htm](npc-gallery/IaSxoVNZFYatdfjI.htm)|Drunkard|Ivrogne|libre|
|[ImdKLPgazv4MSI0F.htm](npc-gallery/ImdKLPgazv4MSI0F.htm)|Barrister|Avocat|officielle|
|[IQJT1Bg9FhvHHEap.htm](npc-gallery/IQJT1Bg9FhvHHEap.htm)|Torchbearer|Porteur de flambeau|officielle|
|[Jg9OEmo68KC91PgC.htm](npc-gallery/Jg9OEmo68KC91PgC.htm)|Teacher|Enseignant|officielle|
|[JsTI2SEZdg2j03gf.htm](npc-gallery/JsTI2SEZdg2j03gf.htm)|Beggar|Mendiant|officielle|
|[K2STan8izudm9eEn.htm](npc-gallery/K2STan8izudm9eEn.htm)|Antipaladin|Anti-paladin|officielle|
|[K8mtLJ5jgxfqxTCv.htm](npc-gallery/K8mtLJ5jgxfqxTCv.htm)|Harrow Reader|Liseur du Tourment|libre|
|[kmHYc2fvhd4QsUEV.htm](npc-gallery/kmHYc2fvhd4QsUEV.htm)|Necromancer|Nécromancien|libre|
|[KPUDfkVpemug2gKj.htm](npc-gallery/KPUDfkVpemug2gKj.htm)|Bandit|Bandit|officielle|
|[KUDsYCHduF0JE3yf.htm](npc-gallery/KUDsYCHduF0JE3yf.htm)|Ship Captain|Capitaine de navire|officielle|
|[KvcFqH6H4TFCuBZA.htm](npc-gallery/KvcFqH6H4TFCuBZA.htm)|Azarketi Crab Catcher|Piégeur de crabe azarketi|libre|
|[ldaY3QcPczuFoqBC.htm](npc-gallery/ldaY3QcPczuFoqBC.htm)|Plague Doctor|Médecin de peste|libre|
|[lemVxzg2Pnx9Nu3d.htm](npc-gallery/lemVxzg2Pnx9Nu3d.htm)|Troubadour|Troubadour|libre|
|[lfXQECIiN0zZdf95.htm](npc-gallery/lfXQECIiN0zZdf95.htm)|Dancer|Danseur|officielle|
|[lTcX8tk6JjQBFcq1.htm](npc-gallery/lTcX8tk6JjQBFcq1.htm)|Zealot of Asmodeus|Zélote d'Asmodeus|libre|
|[M2Vi2mkwMZv1ZRka.htm](npc-gallery/M2Vi2mkwMZv1ZRka.htm)|Tempest-Sun Mage|Mage de la Tempête solaire|libre|
|[mblLgQ9NMR2mMI99.htm](npc-gallery/mblLgQ9NMR2mMI99.htm)|Reckless Scientist|Savant fou|libre|
|[mJxgYD8TQg1W2oXC.htm](npc-gallery/mJxgYD8TQg1W2oXC.htm)|Hellknight Paravicar|Paravicaire des Chevaliers infernaux|libre|
|[ny37LcdsPLY9Osby.htm](npc-gallery/ny37LcdsPLY9Osby.htm)|Saboteur|Saboteur|libre|
|[o4XTf77fEEoFVTdA.htm](npc-gallery/o4XTf77fEEoFVTdA.htm)|Pathfinder Venture-Captain|Capitaine-Aventurier des Éclaireurs|libre|
|[o9dAbSVn4Vi4ejjc.htm](npc-gallery/o9dAbSVn4Vi4ejjc.htm)|Smith|Forgeron|libre|
|[OSCpJYTr6xNIxqZi.htm](npc-gallery/OSCpJYTr6xNIxqZi.htm)|Server|Serveur|officielle|
|[p94aKz7KsiAQJscm.htm](npc-gallery/p94aKz7KsiAQJscm.htm)|Prisoner|Prisonnier|officielle|
|[PLOfWPKwB7pE4arv.htm](npc-gallery/PLOfWPKwB7pE4arv.htm)|Innkeeper|Aubergiste|officielle|
|[pMKrTXmrzDOc9avN.htm](npc-gallery/pMKrTXmrzDOc9avN.htm)|Cult Leader|Chef de culte|libre|
|[PoIuzIWFnlAQ8pdH.htm](npc-gallery/PoIuzIWFnlAQ8pdH.htm)|Beast Tamer|Dompteur|libre|
|[pZOgcQRwXrX9g0s8.htm](npc-gallery/pZOgcQRwXrX9g0s8.htm)|Guildmaster|Maître de guilde|officielle|
|[QAodADCKmbkf53CE.htm](npc-gallery/QAodADCKmbkf53CE.htm)|Librarian|Bibliothécaire|officielle|
|[QZmckb7O3PNgY7D6.htm](npc-gallery/QZmckb7O3PNgY7D6.htm)|Dockhand|Débardeur|officielle|
|[R5SWtNtQt8l7WLYk.htm](npc-gallery/R5SWtNtQt8l7WLYk.htm)|Noble|Noble|officielle|
|[rsATu823vatRe7QJ.htm](npc-gallery/rsATu823vatRe7QJ.htm)|Guard|Garde|officielle|
|[saUg5rtaO9kI9Vir.htm](npc-gallery/saUg5rtaO9kI9Vir.htm)|Harbormaster|Capitaine de port|officielle|
|[sKSfQmJMEsj8QN12.htm](npc-gallery/sKSfQmJMEsj8QN12.htm)|Adept|Adepte|officielle|
|[SwjcZsbkcq6PhiXc.htm](npc-gallery/SwjcZsbkcq6PhiXc.htm)|Spy|Espion|libre|
|[sZ9RwN8zIzpztW3N.htm](npc-gallery/sZ9RwN8zIzpztW3N.htm)|Azarketi Tide Tamer|Dompteur aquatique azarketi|libre|
|[t7QwdZ2m7AbuRWqd.htm](npc-gallery/t7QwdZ2m7AbuRWqd.htm)|Captain Of The Guard|Capitaine de la garde|officielle|
|[tbiThWX0gAVZAMor.htm](npc-gallery/tbiThWX0gAVZAMor.htm)|Judge|Juge|officielle|
|[TCzxsJQjUpy02CsJ.htm](npc-gallery/TCzxsJQjUpy02CsJ.htm)|Tracker|Pisteur|officielle|
|[TgeUj2IiyoTeZHIO.htm](npc-gallery/TgeUj2IiyoTeZHIO.htm)|Watch Officer|Agent du guet|libre|
|[Tj03FbN4SSr0o953.htm](npc-gallery/Tj03FbN4SSr0o953.htm)|Acrobat|Acrobate|officielle|
|[u3tXaX3sOtCvuHW3.htm](npc-gallery/u3tXaX3sOtCvuHW3.htm)|Farmer|Fermier|officielle|
|[UuPPceVcGk1RwSbB.htm](npc-gallery/UuPPceVcGk1RwSbB.htm)|Hellknight Armiger|Écuyer des Chevaliers infernaux|officielle|
|[VkG5yl9xcmziwpQD.htm](npc-gallery/VkG5yl9xcmziwpQD.htm)|Pathfinder Field Agent|Agent de terrain des Éclaireurs|officielle|
|[vkLhqX5oR1t89puZ.htm](npc-gallery/vkLhqX5oR1t89puZ.htm)|Gang Leader|Chef de gang|officielle|
|[w4VJ6h4mysbpdoN4.htm](npc-gallery/w4VJ6h4mysbpdoN4.htm)|Advisor|Conseiller|libre|
|[W9lhKuDeS670LzLx.htm](npc-gallery/W9lhKuDeS670LzLx.htm)|Prophet|Prophète|libre|
|[wfsT2QDtQhsFXQfE.htm](npc-gallery/wfsT2QDtQhsFXQfE.htm)|Surgeon|Chirurgien|officielle|
|[WTCFE1BYdZGWJHh7.htm](npc-gallery/WTCFE1BYdZGWJHh7.htm)|Gravedigger|Fossoyeur|officielle|
|[X1cSs1jhTtx1zTI4.htm](npc-gallery/X1cSs1jhTtx1zTI4.htm)|Poacher|Braconnier|officielle|
|[X7LmMMEOFUUicQ2O.htm](npc-gallery/X7LmMMEOFUUicQ2O.htm)|Guide|Guide|officielle|
|[XpkGaDlyMH2V5wxR.htm](npc-gallery/XpkGaDlyMH2V5wxR.htm)|Priest of Pharasma|Prêtre de Pharasma|libre|
|[xY2WjwebqTNXAP0q.htm](npc-gallery/xY2WjwebqTNXAP0q.htm)|Commoner|Roturier|officielle|
|[Za701s0CV37YPOyo.htm](npc-gallery/Za701s0CV37YPOyo.htm)|Executioner|Bourreau|officielle|
|[Zd0K8TOkthc4a4l7.htm](npc-gallery/Zd0K8TOkthc4a4l7.htm)|Grave Robber|Pilleur de tombes|officielle|
|[zQfufnnLCTzQ165S.htm](npc-gallery/zQfufnnLCTzQ165S.htm)|Monster Hunter|Chasseur de monstres|officielle|
