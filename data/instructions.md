# Instructions pour l'édition des fichiers

* **Name**: le nom original en anglais. Ne pas modifier ce texte ni retirer la ligne!
* **Nom**: le nom en français. En l'absence de traduction, il faut le compléter
* **État**: il s'agit de l'état de la traduction. Ce champ Peut prendre l'une des valeurs suivantes:
  * *aucune*: la traduction est manquante et est à faire
  * *libre*: la traduction est libre, c'est à dire réalisée par les contributeurs du projet
  * *officielle*: la traduction est issue d'une source officielle en vf, c'est à dire de la traduction de l'équipe BBE
  * *changé* cette entrée indique que le fichier anglais d'origine dont l'extraction est reprise a évolué entre deux publications du système. Cela peut être une modification dans les liens ajoutés ou modifiés, le texte en vo qui a été erratisé ou complété, une automatisation ajoutée pour permettre de lancer les dés directement sous Foundry, un effet qui a été ajouté. Lorsque c'est le cas, cela nécessite une vérification et une reprise des modifications en français pour qu'elles correspondent. Si on modifie le fichier pour reprendre les modifiations, il faut alors supprimer le champ ci-dessous et remplacer par l'ancienne valeur libre ou officielle trouvé dans l'État antérieur.
* **Changés**: Ce champ n'existe que s'il y a eu des changements et indique dans quelle partie du fichier le changement a été opéré. 
* **État d'origine**: ce champ n'existe que s'il y a au dessus un état _changé_. Ce champ peut prendre l'une des deux valeurs libre ou officielle. Cela signifie qu'avant le changement, le texte provenait d'une traduction libre ou d'une traduction officielle. En cas de changement dans le texte, l'état passe à libre. Les simples changements de format ou de balises ne font pas perdre le caractère officiel d'une traduction.
* **Desc (en)**: la description d'origine en anglais. Ne pas toucher le texte ou le supprimer. Il sert à vérifier s'il y a des changements entre deux versions du système et permet de les comparer si c'est le cas.
* **Desc (fr)**: la description en français. Sauf si vous savez ce que vous faites et connaissez les balises, il faut se borner à reprendre les balises anglaises. Les balises sont des instructions qui sont données qui permettent la mise en page du texte.
* **Lists**: il s'agit d'autres champs qui seront présentés d'abord en vo puis en vf. On ne touche pas à la vo, on traduit la vf. Si un champ a été changé, il apparaîtra dans les changés.


Exemple de fichier à traduire:

```yaml
Name: Cast a Spell
Nom: 
État: aucune

------ Description (en) ------
<p>You cast a spell you have prepared or in your repertoire. Casting a Spell is a special activity that takes a variable number of actions depending on the spell,... </p>
------ Description (fr) ------

```
Il suffit de remplir la traduction. en dessous de la partie description en français.

Cela donnera 
```yaml
Name: Cast a Spell
Nom: Lancer un sort
État: officielle ou libre

------ Description (en) ------
<p>You cast a spell you have prepared or in your repertoire. Casting a Spell is a special activity that takes a variable number of actions depending on the spell, as listed in each spell's stat block. As soon as the spellcasting actions are complete, the spell effect occurs.

------ Description (fr) ------
<p>Vous lancez un sort que vous avez préparé ou qui figure dans votre répertoire. Lancer un sort est une activité spéciale qui demande un nombre d’actions variable en fonction du sort. Ce nombre figure dans le bloc de statistiques de chaque sort. L’effet du sort se produit dès que les actions d’incantation sont accomplies. </p>

```


Exemple de fichier traduit à partir de la source officielle qui a été changé dans la partie description.

```yaml
Name: Prismatic Plate
Nom: Cuirasse prismatique
État: changé
Changés: desc
État d'origine: libre

```

Lorsque vous avez ceci, c'est qu'il y a eu une mise à jour du fichier anglophone entre deux versions publiées du système. Ce changement a été détecté en générnt les fichiers en vo et l'état est passé à changé.
La ligne Changés indique que le changement inteervenu se trouve dans la partie desc (description).
L'état d'origine vous indique qu'il s'agissait d'une traduction libre auparavant.
Dans ce cas, il faut trouver le changement et le traduire puis changer les états pour montrer que c'est fait.

Avant de pousser les modifications vers le projet, le début du fichier devra être celui-ci

```yaml
Name: Prismatic Plate
Nom: Cuirasse prismatique
État: libre

```