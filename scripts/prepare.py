#!/usr/bin/python3
# -*- coding: utf-8 -*-

from libdata import *


def same_as_compendium(obj, path, value):
    return ((fullSourceId := getValue(obj, "flags.core.sourceId")) is not None
            and (sourceId := fullSourceId[fullSourceId.rfind('.') + 1:]) in referenceItems
            and (referenceItem := referenceItems[sourceId]) is not None
            and (referenceItemValue := getValue(referenceItem, path)) is not None
            and value == referenceItemValue)


def extract_fields(obj, fields):
    extract = {}
    for key, path in fields.items():
        if (value := getValue(obj, path, False)) is not None and value != "":
            extract[key] = {"en": value}
    return extract


def extract_lists(obj, lists):
    extract = {}
    for key, path in lists.items():
        if len((value_list := getList(obj, path))) > 0:
            extract[key] = {"en": value_list}
    return extract


def extract_submapping(obj, subMappings):
    extract = {}
    for key, value in subMappings.items():
        if subMapping := getObject(obj, value["base"]):
            extractMappingValue = {}
            if isinstance(subMapping, list):
                subMapping = {str(k):v for k,v in enumerate(subMapping)}
            for subMappingKey, subMappingValue in subMapping.items():
                extractSubMappingValue = {}
                for field_key, field_path in value["fields"].items():
                    if (submapping_value := getValue(subMappingValue, field_path)) is not None and not submapping_value.startswith("PF2E."):
                        extractSubMappingValue[field_key] = {"en": submapping_value}
                if len(extractSubMappingValue) > 0:
                    extractMappingValue[subMappingKey] = extractSubMappingValue
            if len(extractMappingValue) > 0:
                extract[key] = extractMappingValue
    return extract


def extract_all(obj, paths, source_check=False, desc_extract=True):
    source = {}
    if (value := getValue(obj, paths["name"])) is not None and value != "":
        if not source_check or not same_as_compendium(obj, paths["name"], value):
            source["name"] = {"en": getValue(obj, paths["name"])}
    if desc_extract and "desc" in paths and (value := getValue(obj, paths["desc"])) is not None and value != "":
        if not source_check or not same_as_compendium(obj, paths["desc"], value):
            source["desc"] = {"en": getValue(obj, paths["desc"])}
    if desc_extract and "gm" in paths and (value := getValue(obj, paths["gm"])) is not None and value != "":
        if not source_check or not same_as_compendium(obj, paths["gm"], value):
            source["gm"] = {"en": getValue(obj, paths["gm"])}
    if "fields" in paths:
        if len((fields := extract_fields(obj, paths['fields'])).keys()) > 0:
            source["fields"] = fields
    if "lists" in paths:
        if len((lists := extract_lists(obj, paths['lists'])).keys()) > 0:
            source["lists"] = lists
    if "subMappings" in paths:
        if len((submappings := extract_submapping(obj, paths['subMappings'])).keys()) > 0:
            source["submappings"] = submappings
    return source


def extract_items(obj, items):
    extract = {}
    for item in obj["items"]:
        # Skills : only specific and variants
        if (item['type'] in ["lore"] and item['name'] in SKILLS
                and ("variants" not in item["system"] or len(item["system"]["variants"]) == 0)):
            continue
        desc_check = item['type'] not in ["spell"]
        extract_item = extract_all(item, items, True, desc_check)
        if len(extract_item.keys()) > 0:
            extract[item["_id"]] = extract_item
    return extract


def extract_pages(obj, pages):
    extract = {}
    for page in obj["pages"]:
        extract_item = extract_all(page, pages, True)
        if len(extract_item.keys()) > 0:
            extract_item["journal"] = obj["name"]
            extract[page["_id"]] = extract_item
    return extract


def has_en_changed(existing_dict, source_dict):
    return (existing_dict is None or source_dict is None
            or not isinstance(existing_dict, dict) or not isinstance(source_dict, dict)
            or "en" not in existing_dict.keys() or "en" not in source_dict
            or str(existing_dict.get("en", "")).replace('\n', '').replace('\r', '').strip()
            != str(source_dict.get("en", "")).replace('\n', '').replace('\r', '').strip())


def recursive_merge(existing_data, source_data, changed=None, prefix=""):
    if changed is None:
        changed = set(existing_data.get("changes", []))
    if "name" in source_data:
        if "name" not in existing_data:
            existing_data["name"] = {}
            changed.add(prefix + "name")
        if has_en_changed(existing_data["name"], source_data["name"]):
            existing_data["name"]["en"] = source_data["name"]["en"]
            changed.add(prefix + "name")
    elif "name" in existing_data:
        del existing_data["name"]

    if "desc" in source_data:
        if "desc" not in existing_data:
            existing_data["desc"] = {}
            changed.add(prefix + "desc")
        if has_en_changed(existing_data["desc"], source_data["desc"]):
            existing_data["desc"]["en"] = source_data["desc"]["en"]
            changed.add(prefix + "desc")
    elif "desc" in existing_data:
        del existing_data["desc"]

    if "gm" in source_data:
        if "gm" not in existing_data:
            existing_data["gm"] = {}
            changed.add(prefix + "gm")
        if has_en_changed(existing_data["gm"], source_data["gm"]):
            existing_data["gm"]["en"] = source_data["gm"]["en"]
            changed.add(prefix + "gm")
    elif "gm" in existing_data:
        del existing_data["gm"]

    if "journal" in source_data:
        existing_data["journal"] = source_data["journal"]
    elif "journal" in existing_data:
        del existing_data["journal"]

    if "fields" in source_data:
        if "fields" not in existing_data:
            existing_data["fields"] = {}
        for key, value in source_data["fields"].items():
            if key not in existing_data["fields"]:
                existing_data["fields"][key] = value
                changed.add(prefix + key)
            if has_en_changed(existing_data["fields"].get(key), value):
                existing_data["fields"][key]["en"] = value["en"]
                changed.add(prefix + key)
        # Archivage fields
        for key in list(existing_data["fields"]):
            if key not in source_data["fields"]:
                del existing_data["fields"][key]
    elif "fields" in existing_data:
        del existing_data["fields"]

    if "lists" in source_data:
        if "lists" not in existing_data:
            existing_data["lists"] = {}
        for key, value in source_data["lists"].items():
            if key not in existing_data["lists"]:
                existing_data["lists"][key] = value
                changed.add(prefix + key)
            if existing_data["lists"].get(key, {}).get("en", []) != value["en"]:
                existing_data["lists"][key]["en"] = value["en"]
                changed.add(prefix + key)
        # Archivage lists
        for key in list(existing_data["lists"]):
            if key not in source_data["lists"]:
                del existing_data["lists"][key]
    elif "lists" in existing_data:
        del existing_data["lists"]

    if "submappings" in source_data:
        # Aucun submapping existant, on copie tout de la source
        if "submappings" not in existing_data:
            existing_data["submappings"] = {}
        for key_sub, submapping in source_data["submappings"].items():
            # Ce submapping n'existe pas, on copie tout de la source
            if key_sub not in existing_data["submappings"]:
                existing_data["submappings"][key_sub] = submapping
                changed.add("%s%s" % (prefix, key_sub))
            else:
                for subkey_sub, sub_submapping in submapping.items():
                    # Cette value de submapping n'existe pas, on copie tout de la source
                    if subkey_sub not in existing_data["submappings"][key_sub]:
                        existing_data["submappings"][key_sub][subkey_sub] = sub_submapping
                        changed.add("%s%s.%s" % (prefix, key_sub, subkey_sub))
                    else:
                        for field_key, field_value in sub_submapping.items():
                            if field_key not in existing_data["submappings"][key_sub][subkey_sub]:
                                existing_data["submappings"][key_sub][subkey_sub][field_key] = field_value
                                changed.add("%s%s.%s.%s" % (prefix, key_sub, subkey_sub, field_key))
                            elif has_en_changed(existing_data["submappings"][key_sub][subkey_sub][field_key], field_value):
                                existing_data["submappings"][key_sub][subkey_sub][field_key]["en"] = field_value["en"]
                                changed.add("%s%s.%s.%s" % (prefix, key_sub, subkey_sub, field_key))
                        for field_key in list(existing_data["submappings"][key_sub][subkey_sub]):
                            if field_key not in source_data["submappings"][key_sub][subkey_sub]:
                                del existing_data["submappings"][key_sub][subkey_sub][field_key]
                for subkey_sub in list(existing_data["submappings"][key_sub]):
                    if subkey_sub not in source_data["submappings"][key_sub]:
                        del existing_data["submappings"][key_sub][subkey_sub]
        for key_sub in list(existing_data["submappings"]):
            if key_sub not in source_data["submappings"]:
                del existing_data["submappings"][key_sub]
    elif "submappings" in existing_data:
        del existing_data["submappings"]

    if "items" in source_data:
        if "items" not in existing_data:
            existing_data["items"] = {}
        for key, value in source_data["items"].items():
            if key not in existing_data["items"]:
                existing_data["items"][key] = {}
            recursive_merge(existing_data["items"].get(key, {}), value, changed, key + "->")
        # Archivage items
        for key in list(existing_data["items"]):
            if key not in source_data["items"]:
                del existing_data["items"][key]

    if len(changed) > 0:
        if "status" in existing_data and existing_data["status"] != "changé":
            existing_data["oldstatus"] = existing_data["status"]
            existing_data["status"] = "changé"
        existing_data["changes"] = list(sorted(changed))


print('Loading packs...')
all_packs = getPacks()
packs = all_packs["packs"]

# Packs Folders
existing_folders = {}
if os.path.isfile("../data/folders.json"):
    existing_folders = json.load(open("../data/folders.json", 'r', encoding='utf8'))
for pack in all_packs["packFolders"]:
    if pack in existing_folders:
        all_packs["packFolders"][pack].update(existing_folders[pack])
with open("../data/folders.json", 'w', encoding='utf-8') as f:
    json.dump(all_packs["packFolders"], f, ensure_ascii=False, indent=4, sort_keys=True)

# Construire les objets de référence
referenceItems = {}
for spell in json.load(open("../packs/spells.json", 'r', encoding='utf8')):
    if spell["_id"] not in ["o0l57UfBm9ScEUMW", "6dDtGIUerazSHIOu"]:
        referenceItems[spell["_id"]] = spell
for equipment in json.load(open("../packs/equipment.json", 'r', encoding='utf8')):
    referenceItems[equipment["_id"]] = equipment
for bestiary_ability in json.load(open("../packs/bestiary-ability-glossary-srd.json", 'r', encoding='utf8')):
    referenceItems[bestiary_ability["_id"]] = bestiary_ability

for p in packs:
    print('Preparing %s pack' % p["id"])
    folderPath = "../data/%s/" % p["id"]
    if not os.path.isdir(folderPath):
        os.mkdir(folderPath)
    prepared = set()

    with open(p["pack"] + "/" + p["id"] + ".json", 'r', encoding='utf8') as f:
        duplic = {}
        for obj in json.load(f):
            source = extract_all(obj, p)
            if "items" in p and "items" in obj:
                source["items"] = extract_items(obj, p['items'])

            # Warning de doublons
            if obj["name"] in duplic:
                print_warning("Duplicated name: %s (%s)\e[0m" % (obj["name"], obj["_id"]))
            else:
                duplic[obj["name"]] = obj["name"]

            # Nom du fichier de traduction
            if "type2" in p and getValue(obj, p['type2']) is not None:
                filename = "%s-%s-%s.htm" % (getValue(obj, p['type1']), getValue(obj, p['type2']), obj["_id"])
            elif "type1" in p:
                filename = "%s-%s.htm" % (getValue(obj, p['type1']), obj["_id"])
            else:
                filename = "%s.htm" % obj["_id"]

            # Lecture du fichier de traduction existant, comparaison, fusion, écriture de la nouvelle version
            existing = fileToData("%s/%s" % (folderPath, filename))
            if existing is None:
                existing = source
                existing["status"] = "aucune"
            else:
                recursive_merge(existing, source)
            dataToFile(existing, "%s/%s" % (folderPath, filename))
            prepared.add(filename)

            if "pages" in p:
                source_pages = extract_pages(obj, p['pages'])
                for source_page_id, source_page in source_pages.items():
                    page_filename = "../data/%s-pages/%s.htm" % (p["id"], source_page_id)
                    existing_page = fileToData(page_filename)
                    if existing_page is None:
                        existing_page = source_page
                        existing_page["status"] = "aucune"
                    else:
                        recursive_merge(existing_page, source_page)
                    dataToFile(existing_page, page_filename)

    # Archives
    if not os.path.exists("../archive"):
        os.makedirs("../archive")
    if not os.path.exists("../archive/%s/" % p["id"]):
        os.makedirs("../archive/%s/" % p["id"])

    all_files = os.listdir(folderPath)
    for fpath in all_files:
        if fpath not in prepared and "_folders" not in fpath:
            print("Archiving %s" % fpath)
            os.replace("%s/%s" % (folderPath, fpath), "../archive/%s/%s" % (p["id"], fpath))

    #Compendium folders
    if "folders" in p and p["folders"]:
        existing_folders = {}
        source_folders = {}
        if os.path.isfile("../data/%s/_folders.json" % p["id"]):
            existing_folders = json.load(open("../data/%s/_folders.json" % p["id"], 'r', encoding='utf8'))
        with open("%s/%s_folders.json" % (p["pack"], p["id"]), 'r', encoding='utf8') as f:
            for folder in json.load(f):
                source_folders[folder["name"]] = ""
                if folder["name"] in existing_folders:
                    source_folders[folder["name"]] = existing_folders[folder["name"]]
        with open("../data/%s/_folders.json" % p["id"], 'w', encoding='utf-8') as f:
            json.dump(source_folders, f, ensure_ascii=False, indent=4, sort_keys=True)





